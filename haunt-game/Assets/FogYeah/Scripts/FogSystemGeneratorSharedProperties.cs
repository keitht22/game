﻿namespace FogSystems
{
    internal class FogSystemGeneratorSharedProperties
    {
        /// <summary>
        /// Space between tiles
        /// </summary>
        internal float SpacerSize = 0;
    }
}