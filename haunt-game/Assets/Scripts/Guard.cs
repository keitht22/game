﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Guard : MonoBehaviour
{
    //Observer vars
    public Transform player;
    public GameEnding gameEnding;

    //Waypoint Patrol vars
    public UnityEngine.AI.NavMeshAgent navMeshAgent;
    public Transform[] waypoints;
    public Collider arms;

    //Observer vars
    bool m_IsPlayerInRange;

    //Waypoint Patrol vars
    bool m_PlayerIsGrabbed;
    int m_CurrentWaypointIndex;


    //Observer Methods
    void OnTriggerEnter (Collider arms)
    {
        if (arms.transform == player)
        {
            m_IsPlayerInRange = true;
        }
    }

    void OnTriggerExit (Collider arms)
    {
        if (arms.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }


    void Start()
    {
        navMeshAgent.SetDestination(waypoints[0].position);  
    }

    // Update is called once per frame
    void Update()
    {
        
        if (m_IsPlayerInRange)
        {   
            //Debug.Log(m_IsPlayerInRange);
            navMeshAgent.speed = 2.5f;
            navMeshAgent.SetDestination(player.position);

            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            if(Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == player)
                {
                    gameEnding.CaughtPlayer();
                }
            }

        }


        if(navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }

    }
}