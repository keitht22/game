﻿using UnityEngine;
using System.Collections;
 
public class SimpleTimer: MonoBehaviour {
 
public float targetTime = 10.0f;

public CanvasGroup text;
 
public void Update(){
 
    targetTime -= Time.deltaTime;
 
    if (targetTime > 0.0f)
    {
        text.alpha = 1.0f;
    } else  {
        text.alpha = 0.0f;
    }
 
    }
}